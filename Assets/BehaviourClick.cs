﻿/*    Copyright 2019 Jaroslaw Filiochowski

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* References:
 * https://answers.unity.com/questions/982576/change-material-color-of-an-object-c.html
 * https://docs.unity3d.com/ScriptReference/Random.html
 * https://answers.unity.com/questions/463453/2d-camera-size-in-orthographic-projection-vs-scale.html
 * https://answers.unity.com/questions/574830/detecting-mouse-click-on-2d-sprite.html
 * https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnMouseDown.html
 */

public class BehaviourClick : MonoBehaviour
{
    public Color myColor = Color.black;
    
    public Renderer target;

    // Start is called before the first frame update
    void Start()
    {
        // create inspector element
        //        gameObject.GetComponent<Renderer>().material.color = new Color(color.r, color.g, color.b, color.a);

        //Get the renderer of the object so we can access the color
        //        rend = GetComponent<Renderer>();
        //Set the initial color (0f,0f,0f,0f)
        target.material.color = myColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            myColor.g = Random.Range(0f, 1f);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            myColor.r = Random.Range(0f, 1f);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            myColor.b = Random.Range(0f, 1f);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            myColor.a = Random.Range(0f, 1f);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            myColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f, 1f, 1f);
        }

        target.material.color = myColor;
    }

    void OnMouseDown()
    {
        myColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f, 1f, 1f);
        target.material.color = myColor;
    }
}
